# LibraryBookSearchEngine 图书馆图书搜索引擎
LibraryBookSearchEngine，link librarys  of all Countries, search all resources of books. 
图书馆图书搜索引擎,在家链接全球148个国家的公共图书馆，学校图书馆，聚合查找搜索引擎，图书网站相关图书资源
计划支持全球197个国家和地区。

<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/LibraryBookSearchEngine.gif" width="750px">
  </a>
</p>

开发背景：
2019年底新冠疫情，基本在长沙岳麓区无法出门开展工作，看书，网上谈生意，找电子书，搜图书，搜云盘，非常麻烦，于是开始
构思设计一款能搜索图书，又能搜索图书馆的桌面软件，于是开始搜集国内外的图书馆资料，集成国内主流的图书搜索引擎，包括豆瓣评论，
搜集了148个国家的主流公共图书馆，大学图书馆网址，建立了一个图书馆库。

开发计划：
图书馆图书搜索引擎V1.0
2020  LibraryBookSearchEngineV1.0 
功能：
1.包含148个国家的主流公共图书馆，大学图书馆网址。
2.包含百度、360、必应、搜狗、淘宝、京东、豆瓣、当当、孔夫子9个图书搜索引擎

2022  LibraryBookSearchEngineV2.0
图书馆图书搜索引擎V2.0

功能：
1.覆盖中国2956高校的开放图书馆。  V2.0
2.支持中国2522个县级以上三级图书馆 V2.0

2022  LibraryBookSearchEngineV3.0
图书馆图书搜索引擎V3.0

功能：
1.在原有148个国家的覆盖到全球共有197个国家的主流公共图书馆，大学图书馆网址库。




<p align="center">
================深度交流================
</p>


<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/LibraryBookSearchEngine.png" width="750px">
  </a>
</p>
<p align="center">
================图书馆图书搜索引擎主页================
</p>


<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/germany.png" width="750px">
  </a>
</p>
<p align="center">
================德国系列================
</p>


<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/french.png" width="750px">
  </a>
</p>
<p align="center">
================法国系列================
</p>


<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/swish.png" width="750px">
  </a>
</p>
<p align="center">
================瑞士系列================
</p>


<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/uk.png" width="750px">
  </a>
</p>
<p align="center">
================英国系列================
</p>



<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/canada.png" width="750px">
  </a>
</p>
<p align="center">
================加拿大系列================
</p>



<p align="center">
  <a href="https://github.com/fenwii/LibraryBookSearchEngine">
    <img src="images/american.png" width="750px">
  </a>
</p>
<p align="center">
================美国系列================
</p>



<p align="center">
=================深度交流==================
</p>

<p align="center">
      人生90%的机会来自于人脉和有效信息
</p>

<p align="center">
================分微矩阵社区================
</p>

<p align="center">
	
	
	[分微开源鸿蒙社区]
	鸿蒙开发者联盟QQ交流群1:184548177
	鸿蒙开发者联盟QQ交流群2:374825209
	鸿蒙开发者交流知识星球：鸿蒙开发者联盟   https://t.zsxq.com/2Nj6UnY 
	
	[分微社区公众号官网]
	微信公众号(fenwii-tec)：分微科技
	Github: https://github.com/fenwii
	Gitee: https://gitee.com/samir
	
	[分微技术社区]
	头条，抖音： ChatGPT中国应用社区
	ChatGPT应用实践知识星球：ChatGPT中国应用社区 https://t.zsxq.com/0af53ee8J
	编程技术交流知识星球：C语言C++汇编研究中心   https://t.zsxq.com/jeYVJuj
	架构师知识星球：架构设计师之家 https://t.zsxq.com/0aWGqWZgQ
	
	[分微优质资源社区]
	高端医疗资源知识星球：高端医疗资源分享 https://t.zsxq.com/0atyTFCqL
	商业互助知识星球：商业资源互助社区    https://t.zsxq.com/08K9BXRXa
	A级人才知识星球：Top1000名校人才圈 https://t.zsxq.com/0aQMP0iFO
	
	[分微教育资源社区]
	教育资源知识星球：高考·考研·留学 https://t.zsxq.com/0a2jDiiLX
	
	[分微新产业社区]
	新材料应用知识星球：新材料应用研究小组 https://t.zsxq.com/0aZGVUmEg
	房车产业知识星球：房车产业链 https://t.zsxq.com/0aZeJJerN

	[分微企业创新社区]
	初创企业运作知识星球：初创企业关键路径研究 https://t.zsxq.com/0aN8lHQa1
	企业融资BP知识星球：BP商业计划书行动组 https://t.zsxq.com/0awCVm9ku
	私有企业标杆分析知识星球：不上市公司研究小组 https://t.zsxq.com/0aOhN1w32
	解决方案知识星球：项目解决方案社区 https://t.zsxq.com/0avAwrxqS
	品牌建设知识星球：新品牌创造社区 https://t.zsxq.com/0aOqVTOXG
	智能制造知识星球：智能制造局 https://t.zsxq.com/0aWNdYMEA
	战略规划知识星球：鸭嘴兽工厂 https://t.zsxq.com/0aXQpnqu5
	华为标杆研究知识星球：跟华为任正非学管理 https://t.zsxq.com/0aPlA3QML
	
	[分微金融资源社区]
	创投信息知识星球：金融创投事件库 https://t.zsxq.com/0aWAcDf6Z
	金融资源知识星球：金融资本周转站 https://t.zsxq.com/0abx9W43B
	投资标杆学习知识星球：跟大道学价值投资 https://t.zsxq.com/0aMKAu5h0
	企业家知识星球：成为老板研究社区 https://t.zsxq.com/0aQBKFHSb
	家族办公室知识星球：1959家族办公室 https://t.zsxq.com/0asDwaE6K
	
	[分微持续成长社区]
	写书实践知识星球：微习惯·写书实践社区 https://t.zsxq.com/0awznsMar
	
	[分微社区联系方式]
	微信小助理号：SocratesSchool

</p>  
</p>  
<p align="center">
================分微矩阵社区================
</p>
<p align="center">
=================深度交流==================
</p>
